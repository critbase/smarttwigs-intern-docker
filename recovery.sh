#!/usr/bin/env bash

# set a cron timer to run this file periodically

CONTAINER_ID="a2c54906b6a5" # change depending on container
CONTAINER_NAME="python-container" # change depending on container
CONTAINER_STATUS=$(podman ps --filter "name=$CONTAINER_NAME" | grep "$CONTAINER_NAME")

if [ ! -z "$CONTAINER_STATUS" ]
then
	echo "Service up; no need to restart"
else
	podman restart $CONTAINER_NAME
fi
