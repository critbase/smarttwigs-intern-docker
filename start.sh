#!/usr/bin/env bash
podman build -t python-app:latest .
podman run -p 8080:5000 --name python-container python-app:latest
