FROM python:3.10.0-bullseye
WORKDIR /app

# Test application depends only on flask
RUN pip3 install flask 

COPY app.py .

ENV FLASK_APP=/app/app

CMD [ "python3", "-m", "flask", "run", "--host=0.0.0.0", "--port=5000" ]